# GDB Test Drive

Download the binary: `wget https://artifacts.picoctf.net/c/87/gdbme`
and make it executable. 

* Run it: Nothing happens, no output, no progress.
* Start debugger. `gdb ./gdbme`
* Change to TUI: `layout asm`
* Start execution: `start` 
* There is a call to `sleep()` in instruction at `*main+99` 
* Set a breakpoint there: `b *main+99`
* Let program run to the breakpoint with `c`
* Step into the function with `s`:

		(gdb) s
		Single stepping until exit from function main,
		which has no line number information.
		__sleep (seconds=100000) at ../sysdeps/posix/sleep.c:34
		(gdb) bt
		#0  __sleep (seconds=100000) at ../sysdeps/posix/sleep.c:34
		#1  0x000055555555532f in main ()

	We don't want to wait for 100000 seconds, do we?

* Use `up` to get back to main
* Skip the 100000 seconds by jumping over the `sleep` call: `jump
  *main+104`
* The program prints out the flag
