# c0rrupt write-up #

## Given ##

File to download:

https://jupiter.challenges.picoctf.org/static/ab30fcb7d47364b4190a7d3d40edb551/mystery

Hint: Try fixing the file header

## Solution ##

`file mystery` says "data". Check file with `bvi`. There are several
typical PNG chunk types visible, in particular the file ends with an
IEND section.

Let's try to repair:

1. First 8 bytes should be `89 50 4E 47 0D 0A 1A 0A` - PNG: After
   that, `file mystery` says it's a png file
2. The IHDR chunk type is corrupted
3. The IDAT chunk type is corrupted
4. the length of the first IDAT chunk needs to be corrected:

	Take the address of the second IDAT chunk type, subtract address
    of the first one and subtract 12 bytes for chunk type (4 bytes),
    length (4 bytes) and crc (4):
	
	```
	0x00010008 - 0x57 - 12
	0xffa5
	```

	Use 0x0000FFA5 as new chunk length for IDAT chunk. 

Summary of the changes:

```
diff <(hexdump mystery) <(hexdump mystery.png )
1c1
< 0000000 6589 344e 0a0d aab0 0000 0d00 2243 5244
---
> 0000000 5089 474e 0a0d 0a1a 0000 0d00 4849 5244
6c6
< 0000050 2452 aaf0 ffaa aba5 4544 7854 ec5e 3fbd
---
> 0000050 2452 00f0 ff00 49a5 4144 7854 ec5e 3fbd
```

After these adjustments, the image displays, and I can see the flag
even though exiftool still complains about a checksum mismatch in the
`pHYs` chunk.

![The mystery](mystery.png)

