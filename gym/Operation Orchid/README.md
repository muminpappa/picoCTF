# Operation Orchid #

https://play.picoctf.org/practice/challenge/285

## Given ##

A disk image: https://artifacts.picoctf.net/c/212/disk.flag.img.gz

## Solution ##

Sleuthkit is of great help here; `mmls disk.flag.img` says 

```
DOS Partition Table
Offset Sector: 0
Units are in 512-byte sectors

      Slot      Start        End          Length       Description
000:  Meta      0000000000   0000000000   0000000001   Primary Table (#0)
001:  -------   0000000000   0000002047   0000002048   Unallocated
002:  000:000   0000002048   0000206847   0000204800   Linux (0x83)
003:  000:001   0000206848   0000411647   0000204800   Linux Swap / Solaris x86 (0x82)
004:  000:002   0000411648   0000819199   0000407552   Linux (0x83)
```

Partition 2 looks like a `/boot` partition, so let's go for partition
4 right away. Already the first attempt is successful:

```
$ fls -o 0000411648 -rp disk.flag.img | grep root/
r/r 1875:	root/.ash_history
r/r * 1876(realloc):	root/flag.txt
r/r 1782:	root/flag.txt.enc
```

There is nothing meaningful at inode 1876. Let's extract flag.txt.enc:

``` bash
icat -o 0000411648 disk.flag.img 1782 > flag.txt.enc
```

`file flag.txt.enc` tells us that this is 

```
flag.txt.enc: openssl enc'd data with salted password
```

Where is the password? Let's check the shell history:

```
$ icat -o 0000411648 disk.flag.img 1875
touch flag.txt
nano flag.txt 
apk get nano
apk --help
apk add nano
nano flag.txt 
openssl
openssl aes256 -salt -in flag.txt -out flag.txt.enc -k unbreakablepassword1234567
shred -u flag.txt
ls -al
halt
```

Yes, there is a password. So we can go ahead and decrypt the file:

```
$ openssl enc -aes256 -d -in flag.txt.enc -out flag.txt
enter AES-256-CBC decryption password:
*** WARNING : deprecated key derivation used.
Using -iter or -pbkdf2 would be better.
bad decrypt
40370875E07F0000:error:1C800064:Provider routines:ossl_cipher_unpadblock:bad decrypt:../providers/implementations/ciphers/ciphercommon_block.c:124:
```

Despite the error message, the flag is decrypted successfully into
flag.txt.
