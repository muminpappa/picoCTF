# Operation Oni Write-up #

https://play.picoctf.org/practice/challenge/284

## Given ##

* A disk image to download,
  https://artifacts.picoctf.net/c/70/disk.img.gz
* A host to ssh into

## Solution ##

Get the disk image and
unzip. [Sleuthkit](http://www.sleuthkit.org/sleuthkit) will do the
rest. Look what's inside using `mmls disk.img`:

```
DOS Partition Table
Offset Sector: 0
Units are in 512-byte sectors

      Slot      Start        End          Length       Description
000:  Meta      0000000000   0000000000   0000000001   Primary Table (#0)
001:  -------   0000000000   0000002047   0000002048   Unallocated
002:  000:000   0000002048   0000206847   0000204800   Linux (0x83)
003:  000:001   0000206848   0000471039   0000264192   Linux (0x83)
```

The partition that starts at 2048 is /boot. Let's look into the other
partition with `fls -o 206848 -rp disk.img | less`. Ok, lots of files. We
need an ssh private key, so 

```
fls -o 206848 -rp disk.img | grep '\.ssh'
d/d 3916:	root/.ssh
r/r 2345:	root/.ssh/id_ed25519
r/r 2346:	root/.ssh/id_ed25519.pub
```

The private key sits on inode 2345, so let's get it:

``` bash
icat -o 206848 disk.img 2345 > key.id
```

Change the permissions of the file so ssh does not refuse to use it -
`chmod 0400 key.id` - and use it to ssh into the machine:

``` bash
ssh -i key.id -p 62156 ctf-player@saturn.picoctf.net
```

There we find the key.
