# like1000 write-up #

https://play.picoctf.org/practice/challenge/81

## Given ##

* File https://jupiter.challenges.picoctf.org/static/52084b5ad360b25f9af83933114324e0/1000.tar
* Hint: Script this

## Solution ##

Looking into the 1000.tar reveals two files: 999.tar and filler.txt

Script:

``` bash
k=1000
while true
	do 
		tar xvf ${k}.tar || break
		let k--
done
```

