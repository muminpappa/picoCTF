# Investigative Reversing 4 #

https://play.picoctf.org/practice/challenge/54

## Given ##

* A binary:
  https://jupiter.challenges.picoctf.org/static/cbab42643a1552d04a78dc2acfe4f930/mystery
* Five bmp images:
  * https://jupiter.challenges.picoctf.org/static/cbab42643a1552d04a78dc2acfe4f930/Item01_cp.bmp
  * https://jupiter.challenges.picoctf.org/static/cbab42643a1552d04a78dc2acfe4f930/Item02_cp.bmp
  * https://jupiter.challenges.picoctf.org/static/cbab42643a1552d04a78dc2acfe4f930/Item03_cp.bmp
  * https://jupiter.challenges.picoctf.org/static/cbab42643a1552d04a78dc2acfe4f930/Item04_cp.bmp
  * https://jupiter.challenges.picoctf.org/static/cbab42643a1552d04a78dc2acfe4f930/Item05_cp.bmp
  
## Solution ##

The five bmp files all have exactly the same size.

However, their `md5sum` differ:

```
md5sum *.bmp
854d9733a4e479fe2b2c25812453b5b5  Item01_cp.bmp
e1849e5c97dc5acad8b8959e7c22180c  Item02_cp.bmp
bd5d9ad18b6b6eac7eb6937c79b6869c  Item03_cp.bmp
48ed5cbf6328620b39cef9c163d51f1a  Item04_cp.bmp
beb60093e84d60ab5a05f9819bc29029  Item05_cp.bmp
```

A pairwise comparison of the files `diff <(hexdump Item01_cp.bmp )
 <(hexdump Item02_cp.bmp )` also reveals that they are binary
 identical except for a short piece:

```
72,78c72,78
< 00007f0 e8e9 e8e8 e9e9 e8e8 e8e8 e9e8 e8e8 e9e9
< 0000800 e8e9 4f4e 4f4f e94f e8e9 e9e8 e8e9 e8e8
< 0000810 e8e8 e9e8 e9e8 e8e8 e9e9 e8e8 e8e8 e9e8
< 0000820 e9e8 e8e8 e9e9 e8e8 e8e8 e8e8 e9e9 e9e8
< 0000830 e8e9 e8e8 e8e8 e9e8 e9e8 e8e8 e9e9 e8e8
< 0000840 e8e8 e8e8 e8e9 e9e8 e8e9 e8e8 e8e8 e9e8
< 0000850 e9e8 e9e9 e9e9 e8e8 e8e8 e8e8 e8e8 e8e8
---
> 00007f0 e8e8 e9e8 e8e9 e8e8 e8e8 e8e8 e8e8 e9e8
> 0000800 e8e9 4f4e 4f4f e84f e8e8 e9e8 e8e9 e8e8
> 0000810 e8e8 e8e8 e8e8 e9e8 e8e9 e8e8 e8e8 e8e8
> 0000820 e8e8 e9e8 e8e9 e8e8 e8e8 e8e8 e8e8 e9e8
> 0000830 e8e9 e8e8 e8e8 e8e8 e8e8 e9e8 e8e9 e8e8
> 0000840 e8e8 e8e8 e8e8 e9e8 e8e9 e8e8 e8e8 e8e8
> 0000850 e8e8 e9e8 e8e9 e8e8 e8e8 e8e8 e8e8 e8e8
```

So now we already know where to look for the hidden data.

Using Ghidra (v10.3.3) to decompile `mystery`, we find the following:

1. The `main()` function that reads in a `flag.txt` file and calls the
   function `encodeAll()`.
2. The function `encodeAll()` that defines file name strings and calls
   `encodeDataInFile()`: 
   
   ``` c++
	void encodeAll(void)

	{
	  undefined8 local_48;
	  undefined8 local_40;
	  undefined4 local_38;
	  undefined8 local_28;
	  undefined8 local_20;
	  undefined4 local_18;
	  char k;

	  local_28 = 0x635f31306d657449;
	  local_20 = 0x706d622e70;
	  local_18 = 0;
	  local_48 = 0x622e31306d657449;
	  local_40 = 0x706d;
	  local_38 = 0;
	  for (k = '5'; '0' < k; k = k + -1) {
		encodeDataInFile(&local_48,&local_28);
	  }
	  return;
	}
   ```
   
   The decompilation seems to suffer from some hick-ups. Let's assume
   that the loop also creates real file names from the stubs provided
   in `local_28` and `local_48`. We can verify this in `gdb` below.
   
3. The function `encodeDataInFile()`:


	``` c++
	void encodeDataInFile(char *from_filename,char *to_filename)
	  {
	  size_t sVar1;
	  char from_byte;
	  char local_2d;
	  int skip_bytes;
	  FILE *to_file;
	  FILE *from_file;
	  int j;
	  int i;
	  int k;
	  int local_c;

	  from_file = fopen(from_filename,"r");
	  to_file = fopen(to_filename,"a");
	  if (from_file != (FILE *)0x0) {
		sVar1 = fread(&from_byte,1,1,from_file);
		local_c = (int)sVar1;
		skip_bytes = 0x7e3;
		for (k = 0; k < skip_bytes; k = k + 1) {
		  fputc((int)from_byte,to_file);
		  sVar1 = fread(&from_byte,1,1,from_file);
		  local_c = (int)sVar1;
		}
		for (i = 0; i < 0x32; i = i + 1) {
		  if (i % 5 == 0) {
			for (j = 0; j < 8; j = j + 1) {
			  local_2d = codedChar(j,(int)*(char *)(*flag_index + flag),(int)from_byte);
			  fputc((int)local_2d,to_file);
			  fread(&from_byte,1,1,from_file);
			}
			*flag_index = *flag_index + 1;
		  }
		  else {
			fputc((int)from_byte,to_file);
			fread(&from_byte,1,1,from_file);
		  }
		}
		...
	```
	
	From the decompiled code, we learn that the first argument is the
    name of the original bmp file, and the second argument the name of
    the one that is written. Moreover, the first 2019 (`0x7e3`) bytes
    of the input file are written unmodified to the output file. After
    that, a loop with `0x32 = 50` iterations follows. Whenever 5
    divides the loop variable, the inner loop is executed, which calls
    the function `codedChar()`, writes the resulting byte to the
    output file, and increases the `flag_index`, i.e. points to the
    next character in the `flag` array. When 5 does not divide the
    loop variable, the byte read from the input bmp file is written
    unmodified to the output file.

4. The function `codedChar()` extracts a bit from the flag byte and
    embeds it as LSB into the byte taken from the original bmp image. 

In order to doublecheck my understanding of the code, in particular
the question marks around `encodeAll()`, I start `gdb ./mystery` and
set a breakpoint:

``` gdb
b *encodeAll+107
```

This allows me to inspect the arguments the function
`encodeDataInFile()` is really called with:

``` gdb
Guessed arguments:
arg[0]: 0x7fffffffdcf0 ("Item05.bmp")
arg[1]: 0x7fffffffdd10 ("Item05_cp.bmp")
arg[2]: 0x7fffffffdd10 ("Item05_cp.bmp")
[------------------------------------stack-------------------------------------]
0000| 0x7fffffffdcf0 ("Item05.bmp")
0008| 0x7fffffffdcf8 --> 0x706d ('mp')
0016| 0x7fffffffdd00 --> 0x7fff00000000 
0024| 0x7fffffffdd08 --> 0x7ffff7f96600 --> 0x0 
0032| 0x7fffffffdd10 ("Item05_cp.bmp")
0040| 0x7fffffffdd18 --> 0x706d622e70 ('p.bmp')
0048| 0x7fffffffdd20 --> 0x0 
0056| 0x7fffffffdd28 --> 0x35007fffffffdda0 
[------------------------------------------------------------------------------]
Legend: code, data, rodata, value

Breakpoint 1, 0x0000555555400a85 in encodeAll ()
gdb-peda$
```

I need to create dummy bmp files (I make them 3000 bytes big) with the
names Item0?.bmp so the program does not fail.

This breakpoint is visited another 4 times, and I can observe that the
numbers in the file name arguments count down from 5 to 1.

To sum up the findings so far:

The binary takes one input file Item0?.bmp, writes 2019 bytes
unmodified to the output file Item0?_cp.bmp, encodes 10 bytes of the
flag over a range of 120 bytes of the bitmap (10 bytes flag spread out
over 8 bytes plus 10 times four unmodified bytes, i.e. 40 + 80), and
writes them to the output file, then it writes the remaining input
bytes unmodified to the output bmp.

Now I'm ready to construct a Python script to extract the flag:

``` python
file_list = ['Item05_cp.bmp', 'Item04_cp.bmp', 'Item03_cp.bmp',
             'Item02_cp.bmp', 'Item01_cp.bmp']

for in_file in file_list:
    with open(in_file, 'rb') as bmpfile:
        bmpfile.seek(2019)
        for i in range(10):
            a = bytearray(bmpfile.read(8))
            slask = bmpfile.read(4) # throw away
            a.reverse()
            for b in a:
                print(b & 1, end='')
```

The `a.reverse()` is needed as the loop in `encodeDataInFile()` calls
`codedChar()` starting with the LSB (0 to 7), so the bits come in the
wrong order.

The printout can be pasted into
[CyberChef](https://gchq.github.io/CyberChef/#recipe=From_Binary('None',8))
to obtain the flag.
