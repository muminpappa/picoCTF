# WebNet0 #

https://play.picoctf.org/practice/challenge/32

## Given ##

* A pcap file
  https://jupiter.challenges.picoctf.org/static/0c84d3636dd088d9fe4efd5d0d869a06/capture.pcap
* A key:
  https://jupiter.challenges.picoctf.org/static/0c84d3636dd088d9fe4efd5d0d869a06/picopico.key
  
## Solution ##

Open the capture file in Wireshark. There are three TLS streams in the
captured data. They can be displayed with the filters `tcp.stream eq
0`, `tcp.stream eq 1` and `tcp.stream eq 2`.

Right-click on one of the TLS segments, choose "Protocol preferences
-> Transport Layer Security -> RSA Keys list" and add the key.

After that, the HTTP over TLS traffic can be decrypted by
Wireshark. "Analyse -> Follow -> HTTP stream" reveals the flag as a
header.
