# WhitePages Write-up #

https://play.picoctf.org/practice/challenge/51

## Given ##

https://jupiter.challenges.picoctf.org/static/74274b96fe966126a1953c80762af80d/whitepages.txt


``` bash
hexdump -v -e '1/1 "%x" "\n"' whitepages.txt  | sort | uniq -c
    582 20
    794 80
    794 83
    794 e2
```

Extract bit planes into separate txt files:

``` bash
for bit in 1 2 3 4 5 6 7 8
	do 
	for s in $(xxd -b -c 1 whitepages.txt | cut -f 2 -d ' ' | cut -b $bit)
		do echo -ne $s
		done > whitepages_${bit}.txt
	done
```

Encode each of the new files with lsb first:

``` bash
for bin in 1 2 3 4 5 6 7 8; do basenc -d --base2lsbf whitepages_${bit}.txt > whitepages_${bit}.lsbf; done
```

Same for msb first encoding. Both variants are just gibberish, also `basenc` throws an error probably because the numbers of bits does not divide into 8.

There must be another way to use the data in the whitepages.


``` bash
hexdump -v -e '1/1 "%u" "\n"' whitepages.txt
```

shows that the character 32 can come independently of the other characters, where as 226, 128 and 131 always come grouped in this order:

```
...
226
128
131
226
128
131
32
32
226
128
131
226
128
131
32
32
226
128
131
226
128
131
226
128
131
32
32
226
128
131
32
226
128
131
32
226
128
131
32
32
...
```

Let's try to interprete 32 as a 1 and any of the others (e.g. 226) as 0:

``` bash
for s in $(hexdump -v -e '1/1 "%u" "\n"' whitepages.txt | grep -E '(32|226)' ); do [ $s -eq 32 ] && echo -ne 1 || echo -ne 0; done
```

Pasting the result into CyberChef with a "From Binary" recipe gives

```
	picoCTF

	SEE PUBLIC RECORDS & BACKGROUND REPORT
	5000 Forbes Ave, Pittsburgh, PA 15213
	picoCTF{not_all_spaces_are_created_equal_c54f27cd05c2189f8147cc6f5deb2e56}
```
