# SideChannel #

https://play.picoctf.org/practice/challenge/298

## Given ##

There's something fishy about this PIN-code checker, can you figure
out the PIN and get the flag? Download the PIN checker program here
[pin_checker](https://artifacts.picoctf.net/c/73/pin_checker).  Once
you've figured out the PIN (and gotten the checker program to accept
it), connect to the master server using `nc saturn.picoctf.net 50562`
and provide it the PIN to get your flag.

Hints:

* Read about "timing-based side-channel attacks."
* Attempting to reverse-engineer or exploit the binary won't help you,
  you can figure out the PIN just by interacting with it and measuring
  certain properties about it.
* Don't run your attacks against the master server, it is secured
  against them. The PIN code you get from the `pin_checker` binary is
  the same as the one for the master server.
  
## Solution ##

The hints are quite clear about the direction - checking runtimes and
hoping for a hint (aka longer run time) when a number in the beginning
of the PIN gets right.

Try out the binary:

```
$ ./pin_checker 
Please enter your 8-digit PIN code:
12345678
8
Checking PIN...
Access denied.
```

So the program expects 8 digits. Let's try to measure some runtimes:

```
time echo 1 | ./pin_checker
Please enter your 8-digit PIN code:
1
Incorrect length.
0,003
```

```
$ time echo 12345678 | ./pin_checker
Please enter your 8-digit PIN code:
8
Checking PIN...
Access denied.
0,143
```

```
time echo 123456789 | ./pin_checker
Please enter your 8-digit PIN code:
9
Incorrect length.
0,002
```

So it seems like the times are different when the number has the wrong
length. Let's focus on PINs with the correct length. A time-based side
channel attack relies on that the algorithm returns as soon as one
digit is incorrect, and does not check the remaining digits. Hence, if
we get the first digit right, the program will check the second as
well and hence, need some more time. A small script will facilitate
this. It loops over the first digit and sets the remaining digits to
0: 

``` python
from timeit import default_timer as timer
from subprocess import Popen
import subprocess

pool = list(range(48, 58)) # ASCII 0..9
REPETITIONS = 4 # to get smoother results

for C in pool:
    pin = bytearray([48]*8)
    pin[0] = C
    print(pin.decode(), end='\t')
    check_duration = 0
    for j in range(REPETITIONS):
        start_time = timer()
        with Popen(["./pin_checker"], stdin=subprocess.PIPE,
                   stdout=subprocess.DEVNULL) as proc:
            proc.stdin.write(pin)
        check_duration += (timer() - start_time)/REPETITIONS
    print(check_duration)
```

The output is:

```
$ python3 timing_attack.py
00000000	0.13417208200553432
10000000	0.13156250776955858
20000000	0.13406535299145617
30000000	0.1343933927710168
40000000	0.2540070255054161
50000000	0.13166195024678018
60000000	0.13194617498083971
70000000	0.1322191622457467
80000000	0.13129414798459038
90000000	0.13099966073059477
```

Looks like the first digit is a 4. This digit can now be hard-coded,
and we loop over the second digit instead, and so on.
