from timeit import default_timer as timer
from subprocess import Popen
import subprocess

pool = list(range(48, 58)) # ASCII 0..9
REPETITIONS = 4 # to get smoother results

for C in pool:
    pin = bytearray([48]*8)
    pin[0] = C
    print(pin.decode(), end='\t')
    check_duration = 0
    for j in range(REPETITIONS):
        start_time = timer()
        with Popen(["./pin_checker"], stdin=subprocess.PIPE,
                   stdout=subprocess.DEVNULL) as proc:
            proc.stdin.write(pin)
        check_duration += (timer() - start_time)/REPETITIONS
    print(check_duration)
