# Investigative Reversing 3 #

https://play.picoctf.org/practice/challenge/83

## Given ##

* A binary,
  https://jupiter.challenges.picoctf.org/static/fd9d5bc48b1a6821ce8128672faf3edf/mystery
* An image,
  https://jupiter.challenges.picoctf.org/static/fd9d5bc48b1a6821ce8128672faf3edf/encoded.bmp
* Hint: You will want to reverse how the LSB encoding works on this
  problem
  
## Solution ##

A `strings mystery` results in:

```
flag.txt
original.bmp
encoded.bmp
No flag found, please make sure this is run on the server
No output found, please run this on the server
Invalid Flag
```

So a valid assumption is that the binary expects the files
original.bmp and flag.txt and writes to encoded.bmp. `exiftool -v
encoded.bmp` does not give additional insight:

```
  ExifToolVersion = 12.40
  FileName = encoded.bmp
  Directory = .
  FileSize = 1507414
  FileModifyDate = 1603737717
  FileAccessDate = 1693913624
  FileInodeChangeDate = 1693913618
  FilePermissions = 33204
  FileType = BMP
  FileTypeExtension = BMP
  MIMEType = image/bmp
  + [BinaryData directory, 40 bytes]
  | BMPVersion = 40
  | ImageWidth = 1765
  | ImageHeight = 852
  | Planes = 1
  | BitDepth = 8
  | Compression = 0
  | ImageLength = 1506336
  | PixelsPerMeterX = 0
  | PixelsPerMeterY = 0
  | NumColors = 0
  | NumImportantColors = 0
```

Let's decompile the binary. Ghidra finds:

1. A `main()` function that
   * (as guessed above) opens three files,
   * copies `0x2d3` bytes unchanged from original.bmp to encoded.bmp,
   * expects 50 bytes of flag,
   * loops from 0 to 99, thereby
	 * in even iterations encoding a byte of the flag into 8 bytes of
	 the bitmap (calling the `codedChar()` function, see below), or
	 * in odd iterations, just copy 1 byte from original.bmp to
       encoded.bmp,
   * copies the remainder of original.bmp unchanged over to
     encoded.bmp.
2. A function `codedChar()` that places the k'th bit of it's second
   argument into the LSB of the 3rd argument:
   
	``` c++
	byte codedChar(int param_1,byte param_2,byte param_3)

	{
	  byte local_20;

	  local_20 = param_2;
	  if (param_1 != 0) {
		local_20 = (byte)((int)(char)param_2 >> ((byte)param_1 & 0x1f));
	  }
	  return param_3 & 0xfe | local_20 & 1;
	}
	```
	
This leads to the following reversing script to extract the flag:

``` python
file_list = ['encoded.bmp']

for in_file in file_list:
    with open(in_file, 'rb') as bmpfile:
        bmpfile.seek(0x2d3)
        for i in range(50):
            a = bytearray(bmpfile.read(8))
            slask = bmpfile.read(1)
            letter = 0
            for k in range(8):
                letter = letter | (a[k] & 1) << k
            print(chr(letter), end='')
```

Remark: As I did Investigative Reversing 4 first, this one was quite obvious.
