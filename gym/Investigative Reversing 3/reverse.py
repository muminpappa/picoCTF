# reverse stego in bmp

file_list = ['encoded.bmp']

for in_file in file_list:
    with open(in_file, 'rb') as bmpfile:
        bmpfile.seek(0x2d3)
        for i in range(50):
            a = bytearray(bmpfile.read(8))
            slask = bmpfile.read(1)
            letter = 0
            for k in range(8):
                letter = letter | (a[k] & 1) << k
            print(chr(letter), end='')
