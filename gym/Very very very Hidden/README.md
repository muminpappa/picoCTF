# Very very very Hidden #

## Given ##

Finding a flag may take many steps, but if you look diligently it
won't be long until you find the light at the end of the tunnel. Just
remember, sometimes you find the hidden treasure, but sometimes you
find only a hidden map to the treasure.

https://mercury.picoctf.net/static/9e74d8eddbb00c7769c21215f542917e/try_me.pcap

Hint 1: I believe you found something, but are there any more subtle
hints as random queries?

Hint 2: The flag will only be found once you reverse the hidden
message.

## Solution ##

/Aborted
