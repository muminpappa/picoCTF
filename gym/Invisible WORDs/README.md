# Invisible WORDs Write-up #

https://play.picoctf.org/practice/challenge/354

## Given ##

Do you recognize this cyberpunk baddie? We don't either. AI art
generators are all the rage nowadays, which makes it hard to get a
reliable known cover image. But we know you'll figure it out. The
suspect is believed to be trafficking in classics. That probably won't
help crack the stego, but we hope it will give motivation to bring
this criminal to justice!

Image: https://artifacts.picoctf.net/c/403/output.bmp

Hint 1: Something doesn't quite add up with this image...

Hint 2: How's the image quality?

## Solution ##

Solved this after some hints from the Discord channel that there is a
zip file in the upper two bytes of each pixel.

`exiftool -v output.bmp` says:

```
MIMEType = image/bmp
  + [BinaryData directory, 124 bytes]
  | BMPVersion = 124
  | ImageWidth = 960
  | ImageHeight = 540
  | Planes = 1
  | BitDepth = 32
  | Compression = 3
  | ImageLength = 2073600
  | PixelsPerMeterX = 11811
  | PixelsPerMeterY = 11811
  | NumColors = 0
  | NumImportantColors = 0
  | RedMask = 31744
  | GreenMask = 992
  | BlueMask = 31
  | AlphaMask = 0
  | ColorSpace = BGRs
  | RenderingIntent = 2
```

The masks (red, gree, blue, alpha) together only use 15 bits:

```
; base(2)
	0b1010
; 31744; 992;31
	0b111110000000000
	0b1111100000
	0b11111
; 31744 + 992 + 31
	0b111111111111111
```

while there are 32 bits per pixel available, and there seems to be
data in these bits. From 0x8A forward where the image data starts
there are two bytes of true image data (the lowest bytes), followed by
two bytes of "payload", in this case "PK":

```
00000000  42 4D 8A A4 1F 00 00 00 00 00 8A 00 00 00 7C 00 BM............|.
00000010  00 00 C0 03 00 00 1C 02 00 00 01 00 20 00 03 00 ............ ...
00000020  00 00 00 A4 1F 00 23 2E 00 00 23 2E 00 00 00 00 ......#...#.....
00000030  00 00 00 00 00 00 00 7C 00 00 E0 03 00 00 1F 00 .......|........
00000040  00 00 00 00 00 00 42 47 52 73 00 00 00 00 00 00 ......BGRs......
00000050  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ................
00000060  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ................
00000070  00 00 00 00 00 00 00 00 00 00 02 00 00 00 00 00 ................
00000080  00 00 00 00 00 00 00 00 00 00 38 67 50 4B 95 52 ..........8gPK.R
00000090  03 04 C6 18 14 00 CE 3D 00 00 10 4A 08 00 6F 56 .......=...J..oV
```

Also, the bytes at 0x90 are `03 04`. Together with the "PK", this
would make a zip file signature. Try to extract these:

``` bash
hexdump -v -e '4/1 "%02x" "\n"'  -s 0x8a < output.bmp | cut -b 5-8 | tr -d '\n' | xxd -p -r > file.bin 
```

What is this file?

```
$ zipinfo file.bin
Archive:  file.bin
Zip file size: 1036800 bytes, number of entries: 1
-rw-r--r--  3.0 unx   448642 tx defN 23-Mar-16 03:27 ZnJhbmtlbnN0ZWluLXRlc3QudHh0
1 file, 448642 bytes uncompressed, 169392 bytes compressed:  62.2%
```

Extract the file:

```
7z x file.bin ZnJhbmtlbnN0ZWluLXRlc3QudHh0

7-Zip [64] 16.02 : Copyright (c) 1999-2016 Igor Pavlov : 2016-05-21
p7zip Version 16.02 (locale=en_US.UTF-8,Utf16=on,HugeFiles=on,64 bits,4 CPUs Intel(R) Core(TM) i7-6600U CPU @ 2.60GHz (406E3),ASM,AES-NI)

Scanning the drive for archives:
1 file, 1036800 bytes (1013 KiB)

Extracting archive: file.bin

WARNINGS:
There are data after the end of archive

--
Path = file.bin
Type = zip
WARNINGS:
There are data after the end of archive
Physical Size = 169598
Tail Size = 867202

Everything is Ok

Archives with Warnings: 1

Warnings: 1
Size:       448642
Compressed: 1036800
```

The file contains text, so use `grep` on it:

```
$ grep pico ZnJhbmtlbnN0ZWluLXRlc3QudHh0 
At that age I became acquainted with the celebrated
picoCTF{w0rd_d4wg_y0u_f0und_5h3113ys_m4573rp13c3_e4f8c8f0}
```

## Findings ##

1. `binwalk` did not detect the embedded zip file, probably because
   the signature is spread out.
2. I was not able to isolate the payload using `stegsolve` ...
