# WPA-ing Out #

https://play.picoctf.org/practice/challenge/237

Given: A `pcap` capture and thge hint that the WPA key is inside
the `rockyou.txt` password list. 

## Solution ##

`cowpatty` is of great help here:

```
cowpatty -r wpa-ing_out.pcap -f ~/Downloads/rockyou.txt -s Gone_Surfing
cowpatty 4.8 - WPA-PSK dictionary attack. <jwright@hasborg.com>

Collected all necessary data to mount crack against WPA/PSK passphrase.
Starting dictionary attack.  Please be patient.

The PSK is "mickeymouse".

297 passphrases tested in 0.59 seconds:  505.98 passphrases/second
```

This worked nicely even on my slow notebook.
