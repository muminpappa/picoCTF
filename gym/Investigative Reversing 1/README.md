# Investigative Reversing 1 #

https://play.picoctf.org/practice/challenge/27

## Given ##

* One binary,
  https://jupiter.challenges.picoctf.org/static/c600f6c1bbe8969aefd6f9da0cbdc01c/mystery
* Three images:
  * https://jupiter.challenges.picoctf.org/static/c600f6c1bbe8969aefd6f9da0cbdc01c/mystery.png
  * https://jupiter.challenges.picoctf.org/static/c600f6c1bbe8969aefd6f9da0cbdc01c/mystery2.png
  https://jupiter.challenges.picoctf.org/static/c600f6c1bbe8969aefd6f9da0cbdc01c/mystery3.png
  
## Solution ##

Using `exiftool -v` I learn that all three images have extra data:

```
  Warning = [minor] Trailer data after PNG IEND chunk
```

The `file` command tells me that the binary is not stripped:

```
mystery: ELF 64-bit LSB pie executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, for GNU/Linux 3.2.0, BuildID[sha1]=1b08f7a782a77a6eeb80d7c1d621b4f16f76200a, not stripped
```

Decompilation of the binary in Ghidra yields:

``` c++
  fread(flag_bytes,0x1a,1,__flag_stream);
  fputc((int)flag_bytes[1],__png3_stream);
  fputc((int)(char)(flag_bytes[0] + '\x15'),__png2_stream);
  fputc((int)flag_bytes[2],__png3_stream);
  local_6b = flag_bytes[3];
  fputc((int)local_33,__png3_stream);
  fputc((int)local_34,__png_stream);
  for (k = 6; k < 10; k = k + 1) {
    local_6b = local_6b + '\x01';
    fputc((int)flag_bytes[k],__png_stream);
  }
  fputc((int)local_6b,__png2_stream);
  for (i = 10; i < 0xf; i = i + 1) {
    fputc((int)flag_bytes[i],__png3_stream);
  }
  for (j = 0xf; j < 0x1a; j = j + 1) {
    fputc((int)flag_bytes[j],__png_stream);
  }
```

This is how the flag looks like:

```
character: picoCTF{??
position:  0123456789
```

This is what happens (although Ghidra seems to miss the true size of
the array, and also skips characters 4 and 5):

* Read 26 bytes of the flag into `flag_bytes`
* Append `flag_bytes[0]` (expected to be the letter p) to
  mystery2.png2
* Append `flag_bytes[1]` and `flag_bytes[2]` to mystery3.png2
* Take `flag_bytes[3]` (should be letter o), add a 1 four times, and
  write the result (s) to mystery2.png2
* Take characters 6, 7, 8 and 9 ("F{??") and append them to mystery.png
* Append characters 10..14 to mystery3.png
* Append characters 15..25 to mystery.png

Let's look at the image files:

```
for s in *.png; do echo $s:; xxd -g 1 -s -16 $s; done
mystery2.png:
0001e865: 1f 56 00 00 00 00 49 45 4e 44 ae 42 60 82 85 73  .V....IEND.B`..s
mystery3.png:
0001e86b: 49 45 4e 44 ae 42 60 82 69 63 54 30 74 68 61 5f  IEND.B`.icT0tha_
mystery.png:
0001e873: 43 46 7b 41 6e 31 5f 39 61 34 37 31 34 31 7d 60  CF{An1_9a47141}`
```

So, we can conclude:

```
flag[15:25] = "1_9a47141}`"
flag[10:14] = "0tha_"
flag[6:9] = "F{An"
```

Flag: `picoCTF{An0tha_1_9a47141}`
