# shark on wire 2 write-up #

## Given ##

https://jupiter.challenges.picoctf.org/static/b506393b6f9d53b94011df000c534759/capture.pcap

No hints

## Solution ##

There is a lot of UDP traffic collected between various IP hosts, and
some of this traffic looks promising. However, the flag is not found
in the payload this time. 

Instead, the flag is encoded in the destination port numbers of the
UDP packets sent from port 22 on 10.0.0.66.

Subtract 5000 from each port number and decode from ASCII in
e.g. CyberChef

``` bash
for s in $(tshark -r capture.pcap -Tfields -e udp.srcport 'udp.port==22')
	do 
		echo $[ $s - 5000 ]
	done
```
