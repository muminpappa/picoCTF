#!/bin/bash

byte=''
k=8

# get 400 bytes out of the image in binary, skip the first 2000 bytes
# cut out the LSB (position 8)
for s in $(xxd -s 2000 -b -c 1 -l 400 encoded.bmp | cut -f 2 -d ' ' | cut -b 8)
do 
    # prepend the new bit to the byte
    # needed because mystery stores bits in reverse order
    byte=$s$byte
    let --k
    if [ $k -eq 0 ]
    then
	# the byte is ready
	k=8
	echo $byte
	byte=''
    fi
done

