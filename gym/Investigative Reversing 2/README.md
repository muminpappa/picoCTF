# Investigative Reversing 2 #

https://play.picoctf.org/practice/challenge/13

## Given ##

* A binary:
  https://jupiter.challenges.picoctf.org/static/1d8e2ff583796340cf3eafbf81bf7b70/mystery
* An BMP image:
  https://jupiter.challenges.picoctf.org/static/1d8e2ff583796340cf3eafbf81bf7b70/encoded.bmp 
  
Hints:

* Try using some forensics skills on the image
* This problem requires both forensics and reversing skills
* What is LSB encoding?

## Solution ##

The BMP does not reveal much. But reversing the binary with Ghidra
helps:

2000 bytes are read from the original file and writen unchanged to
encoded.bmp:

``` c++
  local_68 = 2000;
  for (j = 0; j < local_68; j = j + 1) {
    fputc((int)orig_char,enc_file);
    sVar1 = fread(&orig_char,1,1,orig_file);
    local_7c = (int)sVar1;
  }
```

Then, 50 characters are read from the flag file and mangled with the
next bytes from the original image:

``` c++
  sVar1 = fread(flag_txt,0x32,1,flag_file);
  local_64 = (int)sVar1;
  if (local_64 < 1) {
    puts("flag is not 50 chars");
                    /* WARNING: Subroutine does not return */
    exit(0);
  }
  for (i = 0; i < 0x32; i = i + 1) {
    for (k = 0; k < 8; k = k + 1) {
      enc_char = codedChar(k,(int)(char)(flag_txt[i] + -5),(int)orig_char);
      fputc((int)enc_char,enc_file);
      fread(&orig_char,1,1,orig_file);
    }
  }
```

This code loops over the flag, and the inner loop along with the
`codeChar()` function basically LSB encodes each flag character.

``` c++
byte codedChar(int param_1,byte param_2,byte param_3)

{
  byte local_20;
  
  local_20 = param_2;
  if (param_1 != 0) {
    local_20 = (byte)((int)(char)param_2 >> ((byte)param_1 & 0x1f));
  }
  return param_3 & 0xfe | local_20 & 1;
}
```

Below Python code shows for the character "p" (0x70) what happens:

``` python
p2 = 0x70 - 5
p3 = 0xe8
for k in range(8):
	p2 = p2 >> (k & 0x1f)
	print( p3 & 0xfe | p2 & 1 )
```

It is important to note that the bits are written to the encoded file
in reverse order, i.e. bit 0 comes first. This needs to be reversed
during decoding:

``` bash
#!/bin/bash

byte=''
k=8

# get 400 bytes out of the image in binary, skip the first 2000 bytes
# cut out the LSB (position 8)
for s in $(xxd -s 2000 -b -c 1 -l 400 encoded.bmp | cut -f 2 -d ' ' | cut -b 8)
do 
    # prepend the new bit to the byte
    # needed because mystery stores bits in reverse order
    byte=$s$byte
    let --k
    if [ $k -eq 0 ]
    then
		# the byte is ready
		k=8
		echo $byte
		byte=''
    fi
done
```

The rest of the job we leave to CyberChef with [this
recipe](https://gchq.github.io/CyberChef/#recipe=From_Binary('Space',8)ADD(%7B'option':'Hex','string':'5'%7D))

Maybe Python would have been the better choice.
