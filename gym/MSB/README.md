# MSB Write-up #

## Given ##

* An image file Downloads/Ninja-and-Prince-Genji-Ukiyoe-Utagawa-Kunisada.flag.png
* The hint that the information might be in the MSB

## Solution ##

Start StegSolve, open the png, skip through the bit planes. A square in the upper part of the image looks like pure noise in the bit 7 planes of all three colors. 

Use data extract, check bit 7 for all three colors, export as binary, extract by row, MSB first, RGB order (i.e. default options).

The resulting file turns out to be an ASCII text. Grep for the flag.
