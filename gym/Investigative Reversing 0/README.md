# Investigative Reversing 0 #

## Given ##

1. https://jupiter.challenges.picoctf.org/static/6e007dc305ebb3d94c2ab361ee0127a6/mystery
2. https://jupiter.challenges.picoctf.org/static/6e007dc305ebb3d94c2ab361ee0127a6/mystery.png
3. Hints:
   * Try using some forensics skills on the image
   * This problem requires both forensics and reversing skills
   * A hex editor may be helpful
   
## Solution ##

`file mystery` says

```
mystery: ELF 64-bit LSB pie executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, for GNU/Linux 3.2.0, BuildID[sha1]=34b772a4f30594e2f30ac431c72667c3e10fa3e9, not stripped
```

so let's try to run it:

```
$ ./mystery 
No flag found, please make sure this is run on the server
Segmentation fault (core dumped)
```

Hmm. Let's take a look at hints in the file:

```
$ strings mystery
/lib64/ld-linux-x86-64.so.2
libc.so.6
exit
fopen
puts
__stack_chk_fail
fputc
fclose
fread
__cxa_finalize
__libc_start_main
GLIBC_2.4
GLIBC_2.2.5
_ITM_deregisterTMCloneTable
__gmon_start__
_ITM_registerTMCloneTable
u/UH
[]A\A]A^A_
flag.txt
mystery.png
No flag found, please make sure this is run on the server
mystery.png is missing, please run this on the server
at insert
;*3$"
...
```

Seems that the executable looks for both `mystery.png` and `flag.txt`?

`exiftool -v mystery.png` tells me that there is data in the file
after `IEND`, so let's inspect the end of the file using `xxd -g 1 -s
-32 mystery.png`:

```
0001e86d: 4e 44 ae 42 60 82 70 69 63 6f 43 54 4b 80 6b 35  ND.B`.picoCTK.k5
0001e87d: 7a 73 69 64 36 71 5f 66 62 35 31 63 38 32 31 7d  zsid6q_fb51c821}
```

There is a trace of the flag at the end of the file. The string is 26
bytes long. What happens if we create a file flag.txt with similar
structure? So, instead of

```
picoCTK.k5zsid6q_fb51c821}
```

```
picoCTF{hijklmno_HIJKLMNO}
```

and run the executable again:

```
$ ./mystery 
at insert
```

It seems like the mystery.png has been modified. There are now two
"flags" at the end:

```
$ xxd -g 1 -s -64 mystery.png 
0001e867: 00 00 00 00 49 45 4e 44 ae 42 60 82 70 69 63 6f  ....IEND.B`.pico
0001e877: 43 54 4b 80 6b 35 7a 73 69 64 36 71 5f 66 62 35  CTK.k5zsid6q_fb5
0001e887: 31 63 38 32 31 7d 70 69 63 6f 43 54 4b 80 6d 6e  1c821}picoCTK.mn
0001e897: 6f 70 71 72 73 6c 5f 48 49 4a 4b 4c 4d 4e 4f 7d  opqrsl_HIJKLMNO}
```

Compare the string in flag.txt with the one at the end of mystery.png:

```
flag.txt:    picoCTF{hijklmno_HIJKLMNO}
mystery.png: picoCTK.mnopqrsl_HIJKLMNO}
```

There seems to be a "translation rule" for the flag string:

* First 6 characters unchanged
* Character 7...16: an offset is added?

Take a closer look at these changed characters (need to fiddle a bit
with the offset due to the string termination in the text file):

```
$ xxd -c 10 -l10 -g 1 -s -20 mystery.png ; xxd -c 10 -g 1 -s -21 -l 10 flag.txt 
0001e893: 4b 80 6d 6e 6f 70 71 72 73 6c  K.mnopqrsl
00000006: 46 7b 68 69 6a 6b 6c 6d 6e 6f  F{hijklmno
```

So, by running `./mystery`, `0x46` becomes `0x4b`, ... , and `0x6e`
becomes `0x73` (i.e. add 5 for the first 9 bytes), but the last one is
different - `0x6f` becomes `0x6c` (i.e. subtract 3).

Translating the 10 characters in the "flag" we found in mystery.png
back according to these rules, we get:

```
png: K.k5zsid6q
txt: F{f0und_1t
```

so the flag is: `picoCTF{f0und_1t_fb51c821}`.
