# ''.join([chr((ord(flag[i]) << 8) + ord(flag[i + 1]))
#          for i in range(0, len(flag), 2)])

with open('enc', mode='r', encoding='UTF-8') as f:
    a = f.read()

for _, c in enumerate(a):
    print(chr(ord(c) >> 8), end='')
    print(chr(ord(c) % 256), end='')
