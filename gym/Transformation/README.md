# Transformation #

Given: 

* A file with name `enc` - it displays as some Asian characters.
* The commands used for encoding?

	``` python
	''.join([chr((ord(flag[i]) << 8) + ord(flag[i + 1]))
		for i in range(0, len(flag), 2)])
	```

So what needs to be done here ist to read in the file and reverse the
process:

``` python
with open('enc', mode='r', encoding='UTF-8') as f:
    a = f.read()

for _, c in enumerate(a):
    print(chr(ord(c) >> 8), end='')
    print(chr(ord(c) % 256), end='')
```
