# Eavesdrop write-up #

https://play.picoctf.org/practice/challenge/264

## Given ##

https://artifacts.picoctf.net/c/133/capture.flag.pcap

All we know is that this packet capture includes a chat conversation
and a file transfer.

## Solution ##

Find the conversation which is on TCP between 10.0.2.4 and 10.0.2.15
(Analyze -> Follow -> TCP Stream in Wireshark):

```
Hey, how do you decrypt this file again?
You're serious?
Yeah, I'm serious
*sigh* openssl des3 -d -salt -in file.des3 -out file.txt -k supersecretpassword123
Ok, great, thanks.
Let's use Discord next time, it's more secure.
C'mon, no one knows we use this program like this!
Whatever.
Hey.
Yeah?
Could you transfer the file to me again?
Oh great. Ok, over 9002?
Yeah, listening.
Sent it
Got it.
You're unbelievable
```

Obviously, the file is sent on port 9002. Copy the payload and save to
file:

```
echo 53616c7465645f5fd30c863ca650da1fff4fbe72a086457e63626beda615c692cdd27026ae7deea6d1e918b3d40a7f46 | xxd -r -p > file.des3
```

Decrypt according to instruction in the chat:

```
openssl des3 -d -salt -in file.bin -out file.txt -k supersecretpassword123
```

The flag is in file.txt.
