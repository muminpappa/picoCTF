# scrambled-bytes write-up #

https://play.picoctf.org/practice/challenge/206

Challenge provides a capture file `capture.pcapng` as well as the
Python script used to send the data, [send.py](send.py).

From the Python script I find that I should look for single bytes sent
from random source ports to a fixed destination port over UDP. 

I analyse the captured data in Wireshark: The relevant traffic that
fits into above pattern is from `172.17.0.2` to `172.17.0.3`. There is
other UDP traffic as well, so I filter that out by applying a display
filter

```
udp and ip.src==172.17.0.2 and ip.dst==172.17.0.3 and not icmp
```

I include the payload column and export the package dissections as csv
to [capture.csv](capture.csv). We're only interested in the payload,
so I cut it out:

``` bash
cut -f 8 -d ',' capture.csv | tr -d '"' > capture_data.csv
```

and remove the header line from [capture\_data.csv](capture_data.csv).

The sending script shows that the order of the bytes has been shuffled
before sending, and each byte has been XORed with a random byte. As
the seed for `random()` can be guessed (the integer of the `time()`
function immediately before the first segment is sent) it should be
possible to replay the random values:

* seed RNG with the seconds of the time stamp of the first segment -
  possible play around with it a bit
* call the random functions in the same order as in the send file to
  keep the state
  
The random source ports are generated to ensure the state of the
random number generator, but can also be printed to check against the
values in the captured network traffic. 
  
The resulting script is in [receive.py](receive.py). This scripts
unscrambles the bytes and reorders in the original order, before
saving the binary to [capture\_data.bin](capture_data.bin) which turns
out to be a PNG file:

![The flag](capture_data.bin)
