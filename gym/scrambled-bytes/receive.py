import random
from time import time

payload_scrambled = bytearray()

with open('capture_data.csv', 'r', encoding='utf-8') as f:
    payload_scrambled += bytes.fromhex(f.read())

# time stamp from first package
random.seed(1614044650)

order = list(range(len(payload_scrambled)))

random.shuffle(order)

payload = bytearray([0]*len(payload_scrambled))

for i, b in enumerate(payload_scrambled):
    sport = random.randrange(65536)
    # print(sport)
    payload[order[i]] = b ^ random.randrange(256)

with open('capture_data.bin', 'wb') as f:
    f.write(payload)
