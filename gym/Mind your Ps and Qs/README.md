# Mind your Ps and Qs #

## Description ##

In RSA, a small e value can be problematic, but what about N? Can you
decrypt this?

https://mercury.picoctf.net/static/12d820e355a7775a2c9129b2622a7eb6/values

## Hint ##

Bits are expensive, I used only a little bit over 100 to save money

Solution:

These are far more than 100 bits. Anyway. I've tried `factor` (on
Linux) but gave up after ~10 minutes of fan activity. A $(x-y)(x+y)$
approach did not work either.

https://www.alpertron.com.ar/ECM.HTM was able to do the
factorization:

	factor1: 2159 947535 959146 091116 171018 558446 546179
	factor2: 658558 036833 541874 645521 278345 168572 231473

	phi: 1422 450808 944701 344261 903748 621562 998783 582944
		057933 890341 955406 374353 056752 914016

Next, I want to use the Euler totient value $\Phi$ to calculate the
inverse of the encryption coefficient, i.e. the decryption coefficient
$d$. The module
[sympy[(https://docs.sympy.org/latest/modules/core.html#sympy.core.numbers.mod_inverse)
has a function for that, no need to re-invent the wheel.

Finally, we can decrypt the ciphertext $c$ to the plaintext $m$ using

$$m = c^d \mod{n}$$.

This gives another big integer, which is converted to a text string
character by character.
