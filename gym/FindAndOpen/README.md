# FindAndOpen Write-up #

https://play.picoctf.org/practice/challenge/348

## Given ##

* A packet capture file
* An encrypted zip file with the flag
* The hint that the password for the zip file is hidden in the packet
  trace
  
## Solution ##

Look through the `pcap` file. Packet 48 looks interesting, the data in
the the Ethernet frame could be base64 encoded. Copy value, paste into CyberChef, choose "From Hex" followed by "From Base64", and retrieve the first part of the flag, which is also the password for the zip file. 

Unzip the flag.

	
