# picoCTF

My exercises etc from picoctf.org

## From playlists ##

### GDB and assembler ###

`disassemble main`

Set breakpoint in function `main`, line 67: `break *main+67`

Set the breakpoint after entering the overflow data.

Run with `run`

Examine stack: `x/128xb $ebp-0x40`

Display a string: `x/s $rbp-0x30`

Look at register `ebp` (the stack base pointer): `info registers ebp`

[X86 instruction set reference](https://c9x.me/x86/)

### Bash ###

List files with built-in functions:

``` bash
for s in */*
	do echo $s
	while IFS= read -r line || [[ -n "$line" ]]
		do printf '%s\n' $line
	done < $s
done
```

```
-n string
              True if the length of string is non-zero
```

### Hexdump and checksums ###

To calculate checksum over a chunk in e.g. a png file:

``` bash
hexdump -v -e ' 1/1 "%02X"' -s 12 -n 17 reset.png | basenc -d --base16 | rhash --simple -
```

* `-v` - do not squeeze
* `-e ' 1/1 "%02X"'` - format pattern, one byte, zero-padded hex
value, uppercase
* `-s 12` - skip first 12 bytes in file
* `-n 17` - read 17 bytes
* `basenc -d --base16` - decode, interpreting input as hex 

[PNG file structure](https://www.w3.org/TR/PNG-Structure.html)

### Python ###

To overwrite return address, observe big-endian (address here is `0x080491f6`): 

``` bash
python3 -c "import sys;
sys.stdout.buffer.write(b'abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqr\xf6\x91\x04\x08')"
```

Pipe a string into `nc`, append a newline to simulate Enter:

``` bash
python3 -c "import sys; sys.stdout.buffer.write(b'abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqr\xf6\x91\x04\x08\x0a')" | nc saturn.picoctf.net 56736
```

Integer has a `to_bytes()` method that can convert an address and
append to a string:

``` python
b'abcd'+(0x080492bf).to_bytes(4,byteorder='big')
```

### pwntools ###

Tutorials: https://github.com/Gallopsled/pwntools-tutorial#readme

### Other tools ###

Hexdump - `xxd`
For editing: `bvi`

Powerful: [Online hexedit](https://hexed.it/) can e.g. calculate
checksums over marked chunks 

#### Steganography ####

LSB - $T3G0 tool (stego, Python) [on Github
](https://github.com/djrobin17/image-stego-tool) 

`stegdetect` - Github, https://stylesuxx.github.io/steganography/

`stegseek`

Stegsolve - `jar` file,
https://github.com/zardus/ctf-tools/blob/master/stegsolve/install

Use to show one channel at a time, can also extract single channels
(bits) from e.g. png file. Extract to binary and analyse with hex
editor or hexdump, e.g. `hexdump -e '33/1 "%c" "\n"' -n 33 concat_v.bin`

`zsteg` (a Ruby based tool) seems to have a good cli to extract stego data 

#### bvi in PNG files####

Hiding files in files:

In PNG files after IEND come 4 bytes of CRC, everything after that can
be extra data. 

#### Exiftool to extract thumbnails ####

Takes all images in current directory and writes included thumbnails
to the same directory:

`exiftool -a -b -W %d%f_%t%-c.%s -preview:all .`

#### Binary diff ####

```
diff <(hexdump binary_file1) <(hexdump binary_file2)
```

#### Crypto ####

https://www.cryptool.org/en/cto/

https://www.dcode.fr/cipher-identifier

Letter statistics:

``` bash
sed -e 's/\(.\)/\1\n/g' < file.txt  | tr 'a-z' 'A-Z' | grep '[A-Z]' | sort | uniq -c | sort -nr
```

#### Sleuthkit ####

`mmls disk.img` to show the partitions

`srch_strings` - find strings in disk image

Find inode for a specific file name in an image:

``` bash
fls -o 2048 -rp dds2-alpine.flag.img  | grep bottom
ifind -o 2048 -f ext -n "/root/down-at-the-bottom.txt" dds2-alpine.flag.img
```

List contents of the found inode:

``` bash
icat -f ext -o 2048 dds2-alpine.flag.img 18291
```

Include the contents of the slack space: `icat -s ...`

## Gym ##

* `printf(var)` makes it possible to read the stack with the help of a
  crafted **format string** if you can control `var`, e.g. `%5$p`
  reads the 5th "argument" on the stack. Example:
  [stonks](gym/stonks/)
  
  More options: [manipulate address on
  stack](https://web.ecs.syr.edu/~wedu/Teaching/cis643/LectureNotes_New/Format_String.pdf) 
  
* 

